/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class ElectrodeTest extends Component {
  constructor () {
    super()
    this.state = {

    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Test App
        </Text>
        <Text style={styles.instructions}>
          Hello! Anyone seeing this?
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#555',
    fontWeight: 'bold'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('ElectrodeTest', () => ElectrodeTest);
